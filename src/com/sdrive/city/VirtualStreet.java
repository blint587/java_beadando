package com.sdrive.city;


public class VirtualStreet extends Street {

    VirtualStreet(int x1, int y1, int x2, int y2) throws IllegalDirectionException {
        super(x1,y1, x2, y2, "VIRTUAL");
    }
}
