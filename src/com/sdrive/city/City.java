package com.sdrive.city;

import com.sdrive.car.Car;
import com.sdrive.car.AutoPilot;
import com.sdrive.util.Point;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


public class City {


    private Street findStreet(Car c ){
        for (Street str : this.streets){
            for(Point p : str.occupiedPoints())
                if(p.equals(c)){
                    return str;
            }
        }
        return null;
    }

    private Street generateStreet(Car c) {
        Street str  = null;
        for (Street cstr : this.streets) {
            for (Point pts : cstr.occupiedPoints()) {
                try {
                    VirtualStreet nstr = new VirtualStreet(c.getX(), c.getY(), pts.getX(), pts.getY());
                    if (str != null) {
                        if (str.getLength() > nstr.getLength()) {
                            str = nstr;
                        }
                    } else {
                        str = nstr;
                    }
                } catch (Street.IllegalDirectionException ignored) {

                }
            }
        }
        return str;
    }


    public int getXsize() {
        return Xsize;
    }

    public int getYsize() {
        return Ysize;
    }

    private final int Xsize;
    private final int Ysize;

    /*
    Storing collections (reference types) don't have a getter or a setter, although are private, by
    providing public or even protected setter or getters would exposes them and violate the idea of encapsulation
    and leaves them open for tempering, causing undesired behavior.
    Providing unused private getters and setters is meaningless.
    */
    private Set<AutoPilot> cars = new HashSet<>();
    private Set<Street> streets = new HashSet<>();
    private List<Crossing> crossings = new LinkedList<>();
    private String[][] grid;

    public Set<Street> getStreetCopies(){
        return new HashSet<Street>(this.streets);
    }
    public List<Car> getCarCopies() {
        List<Car> ccars = new LinkedList<Car>();
        for (AutoPilot a : this.cars){
            ccars.add(a.getMyCar());
        }
        return ccars;
    }

    private void setDefault(){
        for(int j = 0; j < grid.length; j++){
            for(int i = 0; i< grid[j].length; i++){
                grid[j][i] = ".";
            }
        }
        for(Street str: streets){
            for(Point p: str.occupiedPoints()){
                grid[grid.length-1 - p.getY()][p.getX()] = str.getDirection().line;
            }
        }
        for(Crossing c : crossings){
            grid[grid.length-1 - c.getY()][c.getX()] = c.toString();
        }
    }
    public void addCar(Car c){
        Street str = findStreet(c);

        if (str == null){
            str = generateStreet(c);
        }

        AutoPilot a = new AutoPilot(c, str);
        cars.add(a);
    }
    public void addStreet(Street s){
        for (Street strs : streets){
            // checking if there are grid points occupied by other streets, meaning there is a crossing
            Set<Point> intersections = new HashSet<>(strs.occupiedPoints());
            intersections.retainAll(s.occupiedPoints());
            if(! intersections.isEmpty()){
                for (Point its: intersections){
                    addCrossing(new Crossing(its));
                }
            }
        }
        streets.add(s);
    }
    private void addCrossing(Crossing c){
        this.crossings.add(c);
    }

    public City(int X, int Y){
        this.Xsize = X;
        this.Ysize = Y;
        grid = new String[Y][X];
        this.setDefault();
    }

    @Override
    public String toString() {
        setDefault();
        for(AutoPilot car : cars){
            if ( 0 <= grid.length-1 - car.getMyCar().getY() && grid.length-1 - car.getMyCar().getY() < grid.length && car.getMyCar().getX() <= grid[0].length ){
                grid[grid.length-1 - car.getMyCar().getY()][car.getMyCar().getX()] = car.getMyCar().toString();
            }
        }
        StringBuilder s = new StringBuilder();
        for (String[] aGrid : grid) {
            for (int i = 0; i < this.Xsize; i++) {
                s.append(aGrid[i]);
            }
            s.append('\n');
        }
        return s.toString();
    }

    public void simulationStep(){
        this.setDefault();
        Set<Point> chobs = new HashSet<>();
        chobs.addAll(this.crossings);
        for(Street s :  this.streets){ // adding street endpoints which are not ending in a crossing
            chobs.add(s.getEndPoint());
        }
        for(AutoPilot cur: cars){
            chobs.add(cur.getMyCar());
        }
        for(AutoPilot a: this.cars){
            if (a.getOnStreet() == null){
                this.cars.remove(a);
            }
            else if(a.isOnVirtual()){
                Set<Street> streets = new HashSet<>(this.streets);
                streets.add(a.getOnStreet());
                chobs.addAll(a.driving(chobs, streets));
            }
            else{
                chobs.addAll(a.driving(chobs, this.streets));
            }
        }
    }
}
