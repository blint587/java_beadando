package com.sdrive.city;
import com.sdrive.util.Vector;
import com.sdrive.util.Point;
import com.sdrive.util.Directions;


public class Street extends Vector {

    //properties
    private final Directions direction;

    String getName() {
        return name;
    }

    private final String name;

    public Directions getDirection(){return this.direction;}

    public class IllegalDirectionException extends Exception{
        public IllegalDirectionException(String s){
            super(s);
        }
    }

    public Street(int x1, int y1, int x2, int y2, String name) throws IllegalDirectionException {
        super(new Point(x1, y1), new Point(x2, y2));

        direction = Directions.findDirection(this);
        if(null == this.direction){
            throw new IllegalDirectionException(this.toString());
        }
        this.name = name;
    }

    public boolean isOn(Point p){
        return (p.getY() == (int)(getSlope() * p.getX() + getShift()))
                && (new Vector(getStartPoint(), p).getLength() <= getLength());
    }

}
