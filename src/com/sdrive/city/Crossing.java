package com.sdrive.city;

import com.sdrive.util.Point;

public class Crossing extends Point {
    /*hashCode and all other setters and getters are sufficient for the implementation
      no need to overwrite
    */

    public Crossing(Point p){
        super(p);
    }

    @Override
    public String toString(){
        return "+";
    }

}
