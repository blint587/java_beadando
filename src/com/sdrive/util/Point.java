package com.sdrive.util;


public class Point {

    private int x;
    private int y;

    // constructors
    public Point(int x, int y){setX(x);setY(y);}
    public Point(Point p){this(p.getX(), p.getY());}

    // getters setters
    public int getX(){return this.x;}
    public int getY(){return this.y;}
    protected void setX(int coordinate){
        this.x = coordinate;
    }
    protected void setY(int coordinate){
        this.y = coordinate;
    }

    @Override
    public String toString(){
        return "(" + x + ", " + y +")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Point)) {
            return false;
        }

        Point point = (Point) o;

        return getX() == point.getX() && getY() == point.getY();
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        return result;
    }
}
