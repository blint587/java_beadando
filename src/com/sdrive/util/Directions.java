package com.sdrive.util;


import com.sun.istack.internal.Nullable;

public enum Directions {
    N (0, 1,"↑", "|"),
    NW (-1, 1, "↖", "\\"),
    W (-1, 0, "←", "-"),
    SW (-1, -1,"↙", "/"),
    S (0, -1, "↓", "|"),
    SE (1, -1, "↘", "\\"),
    E (1, 0,"→", "-"),
    NE (1,1,"↗", "/");

    public final Vector direction;
    public final String arrov;
    public final String line;

    Directions(int x, int y, String ar, String ln) {
        direction = new Vector( new Point(0,0), new Point (x, y));
        arrov = ar;
        line = ln;
    }

    @Override
    public String toString() {
        return arrov;
    }

    @Nullable
    public static Directions findDirection(Vector v){
        for( Directions d : Directions.values() ){
            if(d.direction.sameDirection(v)){
                return d;
            }
        }
        return null;
    }
}
