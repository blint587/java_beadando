package com.sdrive.util;

import java.util.HashSet;
import java.util.Set;

public class Vector {

    // properties
    final private Point startPoint;
    final private Point endPoint;

    final private double slope;
    final private double shift;

    // getters
    protected Point getStartPoint() { return startPoint; }
    public Point getEndPoint() {
        return endPoint;
    }
    public double getLength(){
        return Math.sqrt(Math.pow(getXComponent(), 2.0) + Math.pow(getYComponent() - startPoint.getY(), 2.0));
    }
    public int getXComponent(){
        return this.getEndPoint().getX() - this.getStartPoint().getX();
    }
    public int getYComponent(){return this.getEndPoint().getY() - this.getStartPoint().getY();}
    protected double getSlope() {return slope;}
    protected double getShift() {return shift;}

    // constructor
    public Vector(Point sPoint, Point ePoint){
        startPoint = sPoint;
        endPoint = ePoint;
        slope = Vector.slope(this);
        shift = Vector.shift(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Vector)) {
            return false;
        }

        Vector vector = (Vector) o;

        return getStartPoint().equals(vector.getStartPoint()) && getEndPoint().equals(vector.getEndPoint());
    }

    @Override
    public int hashCode() {
        int result = getStartPoint().hashCode();
        result = 31 * result + getEndPoint().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "[" + startPoint + endPoint + "]";
    }

    private static double slope(Vector v){
        return ((double)v.getEndPoint().getY() - (double)v.getStartPoint().getY())/
                ((double)v.getEndPoint().getX() - (double)v.getStartPoint().getX());

    }
    private static double shift(Vector v){
        return (double)v.getStartPoint().getY() - Vector.slope(v) * (double)v.getStartPoint().getX();
    }
    private boolean parallelWith(Vector v){return this.slope == v.slope;}
    public boolean alignsWith(Vector v){
        return parallelWith(v) && v.shift == this.shift;
    }
    public boolean sameDirection(Vector v){return parallelWith(v) && v.getXUnit() == this.getXUnit() && v.getYUnit() == this.getYUnit();}
    private int getXUnit(){return this.getXComponent() != 0 ? this.getXComponent() / Math.abs(this.getXComponent()) : 0;}
    private int getYUnit(){return this.getYComponent() != 0 ? this.getYComponent() / Math.abs(this.getYComponent()) : 0;}

    public Set<Point> occupiedPoints(){
        Set<Point> ocps = new HashSet<>();
        Point ocp = new Point(getStartPoint());

        while(!ocp.equals(getEndPoint())){
            ocps.add(ocp);
            ocp = new Point(ocp.getX() + getXUnit(), ocp.getY() + getYUnit());
        }
        ocps.add(new Point(getEndPoint()));

        return ocps;
    }

}
