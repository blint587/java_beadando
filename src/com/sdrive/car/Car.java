package com.sdrive.car;

import com.sdrive.util.Point;
import com.sdrive.util.Vector;
import com.sdrive.util.Directions;

public class Car extends Point {

    public enum Rep {
        POSITION,
        DIRECTION,
        SPEED,
        ID
    }

    // Static attributes
    private static final int speedLimit = 10;
    private static int Count = 0;
    private static Rep r = Rep.SPEED;

    public static void setRep(Car.Rep r){
        Car.r = r;
    }

    // Instance attributes
    private Directions dir = Directions.N;
    private int speed = 0;
    private final int ID;

    // Getters Setters
    public Directions getDir() {
        return dir;
    }
    void setDir(Directions dir) {
        this.dir = dir;
    }
    public int getSpeed() {
        return speed;
    }
    private void setSpeed(int speed) {
        if (speed >= 0 && speed <= Car.speedLimit ){
            this.speed = speed;
        }else{
            throw new RuntimeException("Speed is out of bounds!");
        }
    }
    private int getID() {
        return this.ID;
    }

    // Driving functionality
    private Point heading(Vector v, int s) {
        return new Point(this.getX() + v.getXComponent() * s, this.getY() + v.getYComponent() * s);
    }

    Point heading() {
        return heading(dir.direction, speed);
    }

    void move(Vector v, int s) {
        Point p = heading(v, s);
        this.setX(p.getX());
        this.setY(p.getY());
    }

    void move() {
        Point p = heading(this.dir.direction, speed);
        this.setX(p.getX());
        this.setY(p.getY());
    }

    void accelerate(int i) {
        if (speed + i <= speedLimit) {
            speed += i;
        }
    }

    void decelerate(int i) {
        if (speed - i >= 0) {
            speed -= i;
        } else {
            stop();
        }
    }

    private void stop() {
        speed = 0;
    }

    public Car(int x, int y, int speed){
        super(x - 1, y - 1);
        setSpeed(speed);
        ID = Count++;
    }

    public Car(int x, int y) {
        this(x, y, 0);
    }

    @Override
    public String toString() {
        String reprez;

        switch (Car.r) {
            case POSITION:
                reprez = super.toString();
                break;
            case DIRECTION:
                reprez = dir.toString();
                break;
            case SPEED:
                reprez = Integer.toString(speed);
                break;
            case ID:
                reprez = "#" + Integer.toString(this.getID());
                break;
            default:
                reprez = super.toString();
                break;
        }
        return reprez;

    }

    @Override
    public int hashCode(){
        return this.getID();
    }
}
