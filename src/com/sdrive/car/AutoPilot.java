package com.sdrive.car;
import com.sdrive.city.Street;
import com.sdrive.city.VirtualStreet;
import com.sdrive.util.Point;
import com.sdrive.util.Vector;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;


/*
The functionality implemented in 'AutoPilot' class could be implemented in 'Car' but due to ints complexity
I saw it more convenient to implement it in a separate class. The two class together complies with specification.
*/

public class AutoPilot {

    //Static properties
    static private Set<Car> CarRegistry = Collections.newSetFromMap(new WeakHashMap<Car, Boolean>());

    //instance properties
    private final Car myCar;

    public boolean isOnVirtual() {
        return onVirtual;
    }

    private void setOnVirtual(boolean onVirtual) {
        this.onVirtual = onVirtual;
    }

    private boolean onVirtual;

    public Car getMyCar() {
        return myCar;
    }

    public Street getOnStreet() {return onStreet;}
    private void setOnStreet(Street onStreet) {
        this.onStreet = onStreet;
        if(onStreet != null){
            this.myCar.setDir(onStreet.getDirection());
            setOnVirtual(onStreet instanceof VirtualStreet);
        }
    }


    private Street onStreet = null;


    private AutoPilot(Car myCar) {
        if (! AutoPilot.CarRegistry.contains(myCar)){ // making sure no one else is driving my car
            this.myCar = myCar;
            AutoPilot.CarRegistry.add(myCar);
        }else{
            throw new RuntimeException(); //TODO: replace with custom exception
        }
    }
    public AutoPilot(Car myCar, Street onStreet){this(myCar);this.setOnStreet(onStreet);}

    //helper methods
    private Vector headingVector(){return new Vector(myCar, myCar.heading());}

    public Set<Point> driving(Set<Point> obstacles, Set<Street> streets) {
        Vector movment = null;
        if (onStreet != null){
            movment = new Vector (myCar.heading(), onStreet.getEndPoint());
        }
        Vector headingV = headingVector();
        for (Point obstacle : obstacles) {
            if (!obstacle.equals(myCar)) {
                Vector tmp = new Vector(myCar.heading(), obstacle);
                if (movment != null &&
                        Math.max(Math.abs(tmp.getXComponent()), Math.abs(tmp.getYComponent())) < Math.max(Math.abs(movment.getXComponent()), Math.abs(movment.getYComponent())) &&
                        headingV.sameDirection(movment)) {
                    movment = tmp;
                } else if (movment == null) {
                    movment = tmp;
                }
            }
        }
        int delta = Math.max(Math.abs(movment.getXComponent()), Math.abs(movment.getYComponent())) - myCar.getSpeed();
        if (delta < 0) {
            myCar.decelerate(-delta);
        } else if (delta > 0) {
            myCar.accelerate(1);
        }

        Set<Point> r = new HashSet<>();
        r.addAll(headingVector().occupiedPoints());
        myCar.move();
        if (myCar.equals(onStreet.getEndPoint())) {
            outerloop:
            for (Street street : streets) {
                for (Point p : street.occupiedPoints()) {
                    if (!street.equals(onStreet) && p.equals(myCar)) {
                        setOnStreet(street);
                        break outerloop;
                    }
                }
            }

        }
        if(myCar.equals(onStreet.getEndPoint())){
            setOnStreet(null);
        }
        return r;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutoPilot)) return false;

        AutoPilot autoPilot = (AutoPilot) o;

        return myCar.equals(autoPilot.myCar);
    }

    @Override
    public int hashCode() {
        return myCar.hashCode();
    }
}
