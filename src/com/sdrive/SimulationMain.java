package com.sdrive;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import com.sdrive.car.Car;
import com.sdrive.city.City;
import com.sdrive.city.Street;



public class SimulationMain {

    private static City buildCitYFromFile(String filePath) throws Exception {
            /*
            The City's constructor is not opening the file directly.
            According to SOLID practice the City it self is a separate class
            and as such it is responsible for handling/storing the Streets
            and Cars and to execute a simulation cycle. Building the City (providing data)
            should be the responsibility of the class / method using the City.
            e.g. Similar tho the builder patter there could be different functionality which are able to
            read data from file or from the console and produce a City instance that can be used regardless
            of the source of the data.
            */
        try(Scanner sc = new Scanner(new FileInputStream(filePath), "UTF-8") ) {

            City city = new City(sc.nextInt(), sc.nextInt());
            while(sc.hasNextLine() && sc.hasNext()){
                String name = sc.next();
                city.addStreet(new Street(sc.nextInt()-1, sc.nextInt()-1, sc.nextInt()-1, sc.nextInt()-1, name));
            }
            return city;
        }
    }

    private static List<Car> readCarsFromFile(String filePath) throws Exception {
        LinkedList<Car> cars = new LinkedList<Car>();
        try (Scanner sc = new Scanner(new FileInputStream(filePath), "UTF-8")) {
            while(sc.hasNextLine() && sc.hasNextInt()){
                cars.add(new Car(sc.nextInt(), sc.nextInt(), sc.nextInt()));
            }
        }
        return cars;
    }

    private static void saveCarsToFile(List<Car> cars, String filename) throws Exception{
        try(PrintWriter pw = new PrintWriter(filename, "UTF-8");) {
            for(Car c : cars){
                pw.println((c.getX()+1) + " " + (c.getY()+1) + " " + c.getSpeed());
            }
            pw.println();
            pw.flush();
        }
    }

    private static Car readCarFromConsole(Scanner r) throws Exception{

        return new Car(r.nextInt(), r.nextInt());
    }


    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        City city = SimulationMain.buildCitYFromFile(args[0]);

        boolean runFlag = true;

        while (runFlag) {
            String instruction = scanner.next();
            switch (instruction) {
                case "sim":
                    city.simulationStep();
                    System.out.println(city);
                    break;
                case "car":
                    city.addCar(SimulationMain.readCarFromConsole(scanner));
                    break;
                case "save":
                    SimulationMain.saveCarsToFile(city.getCarCopies(), scanner.next());
                    break;
                case "load":
                    List<Car> cars = readCarsFromFile(scanner.next());
                    for (Car c : cars) {
                        city.addCar(c);
                    }
                    break;
                case "show":
                    String subninstruction = scanner.next();
                    if (subninstruction.equals("dir")) {
                        Car.setRep(Car.Rep.DIRECTION);
                    } else if (subninstruction.equals("speed")) {
                        Car.setRep(Car.Rep.SPEED);
                    }
                    System.out.println(city);
                    break;
                case "done":
                    runFlag = false;
                    break;
            }
        }
    }
}


