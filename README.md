Készíts olyan programot, amely önvezető autókat szimulál a következőképpen.

Mindegyik osztálynak legyenek meg a szokásos műveletei (toString, equals, hashCode), valamint az adattagokhoz getterek
 és setterek, illetve értelemszerűen konstruktor(ok).
Az autók (car.Car) egy városban (city.City) közlekednek, amelynek kiterjedéseit x és y adattagja írja le: az érvényes 
koordináták (1,1) és (x,y) közöttiek.

A városban utcák vannak (city.Street), amelyek csomópontok (city.Crossing) között futnak. Az utcák iránya lehet N, NW, W, SW, S, SE, E vagy NE, ezek 
a util.Direction felsorolási típus elemei. Az utca konstruktora kapja meg a kezdő- és végpontja koordinátáit 
(x1, y1,  x2, y2); ha ezekből nem állapítható meg az utca iránya (nem pontosan függőlegesen, vízszintesen vagy átlósan halad), 
akkor a konstruktor dobjon IllegalDirection kivételt, amely tartalmazza a kezdő- és végpont koordinátáit. 
Az autóknak szintén van iránya (heading), és emellett sebessége (speed).

A városnak legyen olyan konstruktora, amely szövegesen, soronként beolvassa a város utcáit (streets).
 Egy sor szóközökkel elválasztva tartalmazza az utca nevét, majd a kezdő- és végpontja koordinátáit.

A SimulationMain osztály tartalmazzon főprogramot, amelynek parancssori paraméterei a város méretei,
 illetve egy fájl neve. A fájl a város utcáinak adatait tartalmazza. Miután a főprogram betöltötte a város utcáit, 
 megkezdődik a szimuláció. Ehhez a főprogram a sztenderd bemenetről vár sorokat, amelyek a következők lehetnek.

car X Y: az (X,Y) koordinátákon új önvezető autó jelenik meg, véletlenszerűen választott iránnyal, és 0 sebességgel
(üres sor): a szimuláció megtesz egy lépést (lásd lent), majd kirajzolja a sztenderd kimenetre a várost
save FÁJLNÉV: a megadott nevű fájlba kimentődik szövegesen a város állapota
load FÁJLNÉV: a save ellentéte
show dir: a kirajzolásban az autókat az irányuk jelzi; a város kirajzolódik a sztenderd kimenetre (lépés megtétele nélkül)
show speed: a kirajzolásban az autókat a sebességük jelzi; a város kirajzolódik a sztenderd kimenetre (lépés megtétele nélkül)
done: a főprogram kilép
A lépés megtétele során a város autói a létrehozásuk/betöltésük sorrendje szerint lépnek.

Ha az autó még nem utcán halad, akkor sorban megvizsgálja, hogy N, NW stb. irányokban el tud-e érni egy utcára. 
(Olyan koordinátára, amely az utca két végpontja között fekszik.) A legelső ilyen irányba beáll az autó, és a 
sebességét 1-re állítja. Ha már 1 a sebessége, akkor lép is egyet ebbe az irányba. Ha nincsen érvényesen választható
 irány, akkor az autó kikerül a szimulációból.
Amikor az autó lépés után eléri valamelyik utcát, akkor annak az irányába fordul. Az autó sebessége álljon ekkor 1-re
. Amennyiben több utca is elérhető az adott ponton, akkor véletlenszerűen választ közülük. Mivel az autónak tudnia kell,
 melyik utcán halad, ezt a street adattagja írja le. (Amíg nincsen utcán, legyen null.)
Ha az autó már utcán halad, lépjen a megfelelő irányba annyit, amekkora a sebessége. Ezután a sebességét növelheti eggyel,
 megtarthatja, vagy csökkentheti tetszőleges mértékben; mindig a leggyorsabb sebességet választja úgy, hogy az 
 utca végére 1 sebességel léphessen.
Ha a fentiek következtében az autó sebessége akkora lenne, hogy olyan pontra kellene érkeznie a következő lépésben,
 ahol jelenleg egy másik autó található, akkor csökkentse úgy a sebességét, hogy elkerülje az ütközést. Előfordulhat,
  hogy ennek hatására az autó sebessége nullára csökken.
Ha a lépés elején az autó sebessége nulla, akkor megpróbálja azt eggyel növelni (ha az nem okoz ütközést.)
A város “kirajzolása” a következőképpen történjen.

Az autók az irányuknak megfelelően a ↑↗→↘↓↙←↖ karakterek egyikével legyenek jelölve, ha a show dir direktíva van
 érvényben (ez az alapértelmezett), illetve a sebességük számjegyével, ha a show speed direktívát adták ki utoljára.
Az utcákat, irányaiknak megfelelően, a \|/- karakterek jelöljék.
A később hozzáadott autók jelenjenek meg legfelül; az utcáknál hasonlóan, a legkésőbbi utca jelenjen meg felül.
 Ha van autó az utcán, annak a karaktere jelenjen meg. A kereszteződések helyén (hacsak nincsen ott autó) a * karakter jelenjen meg.
Ahol nincsen se utca, se autó, oda . kerüljön.
A mentés és töltés fájljának szerkezete tetszőlegesen megválasztható, feltéve, hogy szöveges a tartalma. 
A mentés metódusa röviden dokumentálja a fájl szerkezetét.